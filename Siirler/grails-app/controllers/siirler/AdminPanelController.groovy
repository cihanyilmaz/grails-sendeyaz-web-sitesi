package siirler


import com.siirler.ArticlesType
import com.siirler.Articles
import com.siirler.Role
import com.siirler.User
import com.siirler.UserRole

import grails.plugin.springsecurity.annotation.Secured
import java.text.SimpleDateFormat




class AdminPanelController {
	def springSecurityService
	@Secured(['ROLE_ADMIN'])
	def index() {

		List users=User.findAll()

		[users:users]
	}

	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
	def addNewUser(){
	}

	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
	def addUser(){

		String username=params.username
		String password=params.password

		def authority=Role.findById("2")
		def user=new User(username:username,password:password).save(flush:true)

		UserRole.create(user,authority)


		redirect(controller:"begin", action: "index")
	}

	@Secured(['ROLE_ADMIN'])
	def allArticle(){

		List allArticle=Articles.findAll()


		[allArticle:allArticle]
	}

	@Secured(['ROLE_ADMIN'])
	def newArticle(){

		List articlesType=ArticlesType.findAll()

		[articlesType:articlesType]
	}

	@Secured(['ROLE_ADMIN'])
	def addArticle(){

		println "a:::"+params.articleList

		def principal = springSecurityService.principal
		String username = principal.username

		println username

		String title =params.title
		String content =params.content

		println title

		//date alma k�sm�
		SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
		String date = sdf.format(new Date());
		println date


		def user=User.findByUsername(username)
		def articleType=ArticlesType.findByName(params.articleList)

		println user.username

		println articleType.name


		def article=new Articles(title:title,content:content, date_created:date)
		user.addToArticles(article)


		articleType.addToArtic(article)


		article.save(flush:true)

		redirect action:"allArticle"


	}
	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
	def logout(){


		request.getSession().invalidate()
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate")
		response.setHeader("Pragma","no-cache")
		response.setDateHeader("Expires", 0)
		redirect(uri:'/')
	}
	@Secured(['ROLE_ADMIN'])
	def deleteUser(){


		def user=User.findById(params.id)
		def authority =Role.findById("2")
		UserRole.remove(user,authority)

		println "username:::"+user.username

		user.delete()
		redirect action:"index"
	}
	@Secured(['ROLE_ADMIN'])
	def editUser(){

		def user =User.findById(params.id)


		[user:user]
	}
	@Secured(['ROLE_ADMIN'])
	def updateUser(){



		def user=User.findById(params.userId)

		user.username=params.username
		user.password=params.password

		user.save()


		redirect action:"index"
	}
	@Secured(['ROLE_ADMIN'])
	def deleteArticle(){

		def article=Articles.findById(params.id)

		article.delete()

		redirect action:"allArticle"
	}
	@Secured(['ROLE_ADMIN'])
	def editArticle(){

		def articlesTypes=ArticlesType.findAll()


		def article=Articles.findById(params.id)

		[article:article,articlesType:articlesTypes]
	}
	@Secured(['ROLE_ADMIN'])
	def updateArticle(){
		def articleType=ArticlesType.findByName(params.articleList)
		def article=Articles.findById(params.articleId)

		article.title=params.title
		article.content=params.content
		articleType.addToArtic(article)
		article.save()

		redirect action:"allArticle"
	}
}
