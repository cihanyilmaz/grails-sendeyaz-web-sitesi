package siirler

import grails.plugin.springsecurity.annotation.Secured
import java.text.SimpleDateFormat

import com.siirler.Articles
import com.siirler.User
import com.siirler.ArticlesType

class UserPanelController {
	def springSecurityService
	
	 @Secured(['ROLE_USER'])
    def index() {
		
		def principal = springSecurityService.principal
			String userId = principal.id

		
		def user = User.get(userId)
		
		
		def articles=user.articles
		
		
		[articles:articles]
		
	}
	
	@Secured(['ROLE_USER'])
	def newArticle(){
		
		List articlesType=ArticlesType.findAll()
		
		[articlesType:articlesType]
		
	}
	@Secured(['ROLE_USER'])
	def addArticle(){
		
		def principal = springSecurityService.principal
		String username = principal.username

		println username

		String title =params.title
		String content =params.content

		println title

		//date alma k�sm�
		SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
		String date = sdf.format(new Date());
		println date
		

		def user=User.findByUsername(username)
		def articleType=ArticlesType.findByName(params.articleList)

		println user.username

		println articleType.name


		def article=new Articles(title:title,content:content, date_created:date)
		user.addToArticles(article)
		   

		articleType.addToArtic(article)


		article.save(flush:true)
		
		redirect action:"index"
		
		
	}
	
	@Secured(['ROLE_USER'])
	def editArticle(){
		def articlesTypes=ArticlesType.findAll()
		
		
		def article=Articles.findById(params.id)
		
		[article:article,articlesType:articlesTypes]
		
	}
	
	@Secured(['ROLE_USER'])
	def updateArticle(){
		def articleType=ArticlesType.findByName(params.articleList)
		def article=Articles.findById(params.articleId)
		
		article.title=params.title
		article.content=params.content
		articleType.addToArtic(article)
		article.save()
		
		redirect action:"index"
		
	}
	
	@Secured(['ROLE_USER'])
	def deleteArticle(){
		
		def article=Articles.findById(params.id)
		
		article.delete()
		
		redirect action:"index"
	}
	
}
