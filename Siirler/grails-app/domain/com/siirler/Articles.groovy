package com.siirler

import java.util.Date;
import org.grails.databinding.BindingFormat

class Articles {
	
	String title
	
	String content
	
	@BindingFormat('dd-mm-yyyy')
	Date date_created
	
	static belongsTo = [user:User,articlesType:ArticlesType]
	
	static mapping = {
		version false
		content sqlType: 'text'
	}
}
