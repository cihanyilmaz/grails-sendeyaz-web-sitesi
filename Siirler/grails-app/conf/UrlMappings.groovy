class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

       // "/"(view:"begin/index")
		"/" ( controller:'begin', action:'index' )
        "500"(view:'/error')
	}
}
