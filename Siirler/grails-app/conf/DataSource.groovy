dataSource {
    pooled = true
    //pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    configClass = HibernateFilterDomainConfiguration
    dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
    driverClassName = "com.mysql.jdbc.Driver"
    username = "root"
    password = "root"
}
hibernate {
	reload=false
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    // cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
//    cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
    //singleSession = true // configure OSIV singleSession mode
    cache.provider_class='org.hibernate.cache.EhCacheProvider'
}

// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:mysql://localhost:3306/mahmut?autoreconnect=true"
        }

    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:mysql://localhost:3306/mahmut?autoreconnect=true"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:mysql://localhost:3306/mahmut?autoreconnect=true"

        }
    }
}

