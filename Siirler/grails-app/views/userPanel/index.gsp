<!DOCTYPE HTML>
<html>
<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

</head>

<body>
<div class="container">
<br><br><br><br>
<div class="row">

   <div class="col-sm-12"><div class="btn-group " role="group" aria-label="...">
 
  <g:link controller="UserPanel" action="index" ><button type="button" class="btn btn-info">Yazılar</button></g:link>
  <g:link controller="AdminPanel" action="logout" ><button type="button" class="btn btn-warning">ÇIKIŞ YAP</button></g:link>

<br> <br>
  
  </div></div>
  
  
  
  <div class="row">
  
   <div class="col-sm-12">
   <div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-heading">Yazılar</div>
  <div class="panel-body">
    <p>SİSTEME HOŞGELDİNİZ</p>
    <g:link controller="UserPanel" action="newArticle" ><button type="button" class="btn btn-default">Yeni Yazı</button></g:link>
    <br><br>
   
   
   </div>
  
  
  
  </div>
  <div class="row">
  
  
  
   <div class="col-sm-12">
   <!-- Table -->
  <table class="table" >
 
<tr>

<th>TİTLE</th>


<th>EDİT</th>
<th>DELETE</th>

</tr>

<g:each in="${articles}" var="article">
<tr>
<td>${article.title}</td>



<td><g:link controller="userPanel" action="editArticle" id="${article.id}">Düzenle</g:link></td>
<td><g:link controller="userPanel" onClick="return confirm('Bu kaydı silmek istediğinizden emin misiniz?')" action="deleteArticle" id="${article.id}">Sil</g:link></td>


</tr>
</g:each>




</table>
  
  
  
  <br><br>
   
   
   
   </div>
  
  </div>
  
   <div class="row">
   
    <div class="col-sm-12">
    
       <g:link controller="begin" action="index" ><button type="button" class="btn btn-info">Ana Sayfa</button></g:link>
    
    
    
      </div>
   
   
   </div>
  
  
  
  </div>

  

</div>




</div>













  
</div>




</body>

</html>






