<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">


		<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
$(function(){
	  //attach a click event to the element with id 'pass'.

		 
	  $('#submit').click(function(){
		 

		  
		  if($('#username').val()=="")
			  {
			  alert("username is not empty");
			  return false;
			  
			  }
				
				 
				
			  if($('#password').val()=="")
			{
				  alert("password is not empty")
					 return false;

					}
		
		
	  });
  });
	  
	

</script>


		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="style.css">
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>Admin</title>
	</head>
	<body>
		<div class="container">
		
		<br><br><br><br><br><br><br><br>
			<div class="row main">
				<div class="main-login main-center">
				<h1>Update User.</h1>
					<g:form  controller="AdminPanel" action="updateUser">
						
					<g:hiddenField name="userId" value="${user.id }" />
						

						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									 <g:textField class="form-control" name="username" id="username" value="${user.username }"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									 <g:textField class="form-control" type="password" name="password" id="password"  value="${user.password }" />
								</div>
							</div>
						</div>

						

						<div class="form-group ">
							<g:submitButton name="Güncelle" id="submit" style="float:center" class="btn btn-primary btn-lg btn-block login-button"/>
							
						</div>
						 </g:form>
					<g:link controller="adminPanel" action="index" ><button type="button" class="btn btn-default">Geri Dön</button></g:link>
				</div>
			</div>
		</div>

		
	</body>
</html>