<!DOCTYPE HTML>
<html>
<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
$(function(){
	  //attach a click event to the element with id 'pass'.

		 
	  $('#submit').click(function(){
		 

		  
		  if($('#title').val()=="")
			  {
			  alert("title is not empty");
			  return false;
			  
			  }
				
				 
				
			  if($('#content').val()=="")
			{
				  alert("content is not empty")
					 return false;

					}
			  if($('#articlesType').val()=="")
				{
					  alert("articlesType is not empty")
						 return false;

						}
		
	  });
  });
	  
	

</script>


</head>

<body>
<script type="text/javascript">


</script>
<div class="container">

<div class="col-sm-12">
<br><br><br><br><br><br><br>
	<div class="row">
	<h2>UPDATE</h2>
		<g:form  controller="AdminPanel" action="updateArticle">
		<g:hiddenField name="articleId" value="${article.id }" />
                    <div class="row">
                  
                		<div class="col-md-6">
                  		<div class="form-group">
                            <input type="text" class="form-control" name="title" autocomplete="off" id="title" placeholder="Title" value="${article.title }">
                  		</div>
                  	</div>
                    	
                  	</div>
                  	<div class="row">
                  	 
                  		<div class="col-md-12">
                  		<div class="form-group">
                            <textarea class="form-control textarea" rows="3" name="content" id="content" placeholder="Article" >${article.content }</textarea>
                  		</div>
                  	</div>
                    </div>
                <g:select id="articlesType" name="articleList" from="${articlesType}" optionKey="name" optionValue="name"/>
                    <div class="row">
                    <div class="col-md-12">
                  <button type="submit" id="submit" class="btn main-btn pull-right">Güncelle</button>
                  </div>
                  </div>
                </g:form>
                
                <g:link controller="adminPanel" action="index" ><button type="button" class="btn btn-default">Geri Dön</button></g:link>
	</div>
</div>


</div>



</body>






</html>