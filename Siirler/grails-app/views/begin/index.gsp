<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

<script>
$(function(){
	

		 
	  $('.m').click(function(){
		
		console.info( $(this).val());
		//var xs=1;
		//console.info(xs)
		//var number = parseInt("1");
	

		  $.ajax({
			    url:"${g.createLink(controller:'begin',action:'getJson')}",
			    dataType: 'text',
			    data: {
			    	id: $(this).val(),
			        
			    },
			    beforeSend: function() {
			        $('#baslik').html("<img src='${resource(dir: 'images', file: 'vv.gif')}' />");
			      },
			    success: function(data) {
			    	
			        //alert(data)
			    	  //$('h4').text(data);
			    	  $('#baslik').text(data)
			    	  
			    },
			    error: function(request, status, error) {
			        alert(error)
			    },
			    complete: function() {
			    }
			});

			
			
	  });
  });
	  
	

</script>

<title>Sende Yaz</title>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>


	<div class="header">
		<div class="wrap">
			<div class="logo">
				<g:link controller="begin" action="index">
					<img src="images/ad.png" style="height: 50px;" alt="crocodile" />
				</g:link>
			</div>
			<div class="headerbox">
				<div class="login">
					<ul>
						<li><g:link controller="AdminPanel" action="index">Admin in</g:link></li>
						<li><g:link controller="UserPanel" action="index">User in</g:link></li>
						<li><g:link controller="AdminPanel" action="addNewUser">Register</g:link></li>
						<sec:ifLoggedIn>

							<br>
							&nbsp; &nbsp;
                        <li style="color: green;">Welcome ${username }<g:link
									controller="AdminPanel" action="logout">Çıkış Yap</g:link></li>

						</sec:ifLoggedIn>

					</ul>
				</div>

			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="wrap">
		<div class="main">
			<div class="nav">
				<ul>
					<li class="active"><g:link controller="begin" action="index">Ana Sayfa</g:link></li>
					<li class="active"><g:link controller="begin"
							action="haberler">Haberler</g:link></li>

				</ul>
				<div class="clear"></div>
			</div>
			<div class="content">





				<g:each in="${articles}" var="article">

					<article>
						<h1 style="font-size: 160%;">
							${article.title }
						</h1>
						<p
							style="font-size: 110%; max-width: 5000px; padding: 10px; display: block; background-color: #ddd; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"
							class="boyut">
							${article.content }

						</p>
						<br>
						<p style="color: red;">
							Türü:${article.articlesType.name }
						</p>


						<br>
						<g:link controller="begin" action="showArticle" id="${article.id}">Devamı</g:link>
					</article>


				</g:each>


			</div>
			<div class="sidebar">



				<div class="row">

					<br>
					<br>
					<br>

					<h2>Son Dakika Haberleri</h2>
					<div id="myCarousel" class="carousel slide" data-ride="carousel">

						<!-- Indicators -->

						<ol class="carousel-indicators">

							<g:set var="t" value="${0}" />

							<g:while test="${t < a.size()}">
								<li data-target="#myCarousel" data-slide-to="${t}"
									class="active"></li>


								<g:set var="t" value="${t+1}" />

							</g:while>
						</ol>



						<!-- Wrapper for slides -->
						<div class="carousel-inner">



							<div class="item active">

								<img src="${resource(dir: 'images', file: 'add.png')}" alt="v"
									style="width: 100%;">

							</div>

							<g:set var="b" value="${0}" />

							<g:while test="${b < a.size()}">


								<div class="item">
									<p style="text-align: center; color: red">
										${list[b] }
									</p>
									<button
										style="margin-left: auto; margin-right: auto; display: block; margin-top: 0%; margin-bottom: 0%"
										value="${b}" id="degeri" type="button"
										class="btn btn-primary btn-lg m" data-toggle="modal"
										data-target="#myModal">Devamı</button>
									<img src="${a[b]}" alt="${list[b] }" style="width: 100%;">
								</div>
								<g:set var="b" value="${b+1}" />

							</g:while>


						</div>






						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel"
							data-slide="prev"> <span
							class="glyphicon glyphicon-chevron-left"></span> <span
							class="sr-only">Previous</span>
						</a> <a class="right carousel-control" href="#myCarousel"
							data-slide="next"> <span
							class="glyphicon glyphicon-chevron-right"></span> <span
							class="sr-only">Next</span>
						</a>
					</div>


				</div>


				<div class="recent">
					<h3>Son Eklenenler</h3>
					<ul>

						<g:each in="${articles}" var="article">
							<li><g:link controller="begin" action="showArticle"
									id="${article.id}">
									${article.title}
								</g:link></li>

						</g:each>

					</ul>
				</div>

				<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel" id="baslika">Haberin
									Devamı</h4>
							</div>
							<div class="modal-body" id="baslik">...</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Kapat</button>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>
